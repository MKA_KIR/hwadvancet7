const request = fetch('https://swapi.dev/api/films/');
request.then(response => response.json())
    .then(result => {
        const {results} = result;
        const ListStr = results.map(({episode_id, title, opening_crawl, characters}) => {
            const movieCard = document.createElement("div");
            movieCard.className = 'poster';
            movieCard.innerHTML = `<div class="poster__info">
                                    <h2 class="poster__title">${episode_id} : ${title}</h2>
                                    <p class="poster__text">${opening_crawl}</p>
                                </div>`;
            const button = document.createElement('a');
            button.className = "show-all-btn";
            button.textContent = 'Get all characters';
            movieCard.insertAdjacentElement('beforeend', button);
            button.addEventListener('click', function (e) {
                e.preventDefault();
                movieCard.insertAdjacentHTML("beforeend", `<div class="all-characters"><h4>Characters list:</h4><ul class="characters-list"></ul></div>`);
                const requests = characters.map(url=>fetch(url));
                Promise.all(requests).
                    then(responses => Promise.all(responses.map(response => response.json()))).
                    then(results=> {
                        const allCharacters = results.map(({name})=> `<li>${name}</li>`).join('');
                        const charactersList = movieCard.querySelector(".characters-list");
                        charactersList.insertAdjacentHTML('beforeend', allCharacters);
                })
            });
            return movieCard;
        });
        const movieList = document.getElementById('movie_list');
        movieList.append(...ListStr);
        }).catch(error => alert("Запрос не удался"));


